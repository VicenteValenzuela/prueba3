/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ciisa.u3.autos.dao;

import cl.ciisa.u3.autos.dao.exceptions.NonexistentEntityException;
import cl.ciisa.u3.autos.dao.exceptions.PreexistingEntityException;
import cl.ciisa.u3.autos.entity.Autosventa;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import javax.persistence.Persistence;

/**
 *
 * @author vvalenzuela
 */
public class AutosventaJpaController implements Serializable {

    public AutosventaJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("autos_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Autosventa autosventa) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(autosventa);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findAutosventa(autosventa.getCodigo()) != null) {
                throw new PreexistingEntityException("Autosventa " + autosventa + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Autosventa autosventa) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            autosventa = em.merge(autosventa);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = autosventa.getCodigo();
                if (findAutosventa(id) == null) {
                    throw new NonexistentEntityException("The autosventa with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Autosventa autosventa;
            try {
                autosventa = em.getReference(Autosventa.class, id);
                autosventa.getCodigo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The autosventa with id " + id + " no longer exists.", enfe);
            }
            em.remove(autosventa);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Autosventa> findAutosventaEntities() {
        return findAutosventaEntities(true, -1, -1);
    }

    public List<Autosventa> findAutosventaEntities(int maxResults, int firstResult) {
        return findAutosventaEntities(false, maxResults, firstResult);
    }

    private List<Autosventa> findAutosventaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Autosventa.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Autosventa findAutosventa(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Autosventa.class, id);
        } finally {
            em.close();
        }
    }

    public int getAutosventaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Autosventa> rt = cq.from(Autosventa.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
