/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ciisa.u3.autos.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vvalenzuela
 */
@Entity
@Table(name = "autosventa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Autosventa.findAll", query = "SELECT a FROM Autosventa a"),
    @NamedQuery(name = "Autosventa.findByCodigo", query = "SELECT a FROM Autosventa a WHERE a.codigo = :codigo"),
    @NamedQuery(name = "Autosventa.findByMarca", query = "SELECT a FROM Autosventa a WHERE a.marca = :marca"),
    @NamedQuery(name = "Autosventa.findByModelo", query = "SELECT a FROM Autosventa a WHERE a.modelo = :modelo"),
    @NamedQuery(name = "Autosventa.findByYear", query = "SELECT a FROM Autosventa a WHERE a.year = :year"),
    @NamedQuery(name = "Autosventa.findByColor", query = "SELECT a FROM Autosventa a WHERE a.color = :color")})
public class Autosventa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "codigo")
    private String codigo;
    @Size(max = 20)
    @Column(name = "marca")
    private String marca;
    @Size(max = 20)
    @Column(name = "modelo")
    private String modelo;
    @Size(max = 4)
    @Column(name = "year")
    private String year;
    @Size(max = 20)
    @Column(name = "color")
    private String color;

    public Autosventa() {
    }

    public Autosventa(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Autosventa)) {
            return false;
        }
        Autosventa other = (Autosventa) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.entity.Autosventa[ codigo=" + codigo + " ]";
    }
    
}
